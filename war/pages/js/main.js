angular.module('FutsalManager',[],function($routeProvider,$locationProvider){
	console.log("bootstrap");
	$routeProvider.when('/PlayerList',{
		templateUrl: '/player_list.html',
		controller: PlayerListCtrl
	});
	$routeProvider.when('/PlayerDetail/:playerKey',{
		templateUrl: '/player_detail.html',
		controller: PlayerDetailCtrl
	});
	$routeProvider.when('/EventList',{
		templateUrl: '/event_list.html',
		controller: EventListCtrl
	});
	$routeProvider.when('/EventDetail/:eventKey',{
		templateUrl: '/event_detail.html',
		controller: EventDetailCtrl
	});
	$routeProvider.when('/TeamList/:eventKey',{
		templateUrl: '/team_list.html',
		controller: TeamListCtrl
	});
	$routeProvider.when('/GameList/:eventKey',{
		templateUrl: '/game_list.html',
		controller: GameListCtrl
	});
	$locationProvider.html5Mode(true);
});

function MainCtrl($scope, $route, $routeParams, $location) {
	console.log("MainCtrl");
	$scope.$route = $route;
	$scope.$location = $location;
	$scope.$routeParams = $routeParams;
}