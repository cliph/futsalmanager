var rootScope = null;
var targetEventKey = "";
function EventListCtrl($scope, $http,$timeout){
	rootScope = $scope;
	$http.get('/event?action=query').success(function(data){
		$scope.events = data;
	});

	var cnt = 0;
	$scope.message = "";
	$scope.refresh = function(){
		$scope.message = "waiting for update:"+cnt;
		cnt = cnt + 1;
		$timeout(function(){
			$http.get('/event?action=query').success(function(data){
				$scope.events = data;
				console.log("data.length:"+data.length);
			});
			var finish = true;
			for(var event in $scope.events){
				console.log("event.keyStr:"+event.keyStr+" targetEventKey:"+targetEventKey);
				if(event.keyStr==targetEventKey){
					console.log("targetExist");
					finish = false;
				}
			}
			if(!finish&&cnt<10){
				console.log("continue:"+cnt);
				$scope.refresh();
			}else{
				console.log("finish:"+cnt);
				$scope.reset();
			}
		},3000);
	}

	$scope.reset = function(){
		$scope.message="";
		targetEventKey="";
		cnt = 0;
	}
	$scope.newEvent = function(){
		var refreshFlg = false;
		$http.get('/event?action=create&eventName='+$scope.newEventName+'&eventDate='+$scope.newEventDate+'&place='+$scope.newEventPlace).
			success(function(data){
				console.log("dataSize:"+data.length);
				targetEventKey = data.keyStr;
				console.log("targetEventKeyNew:"+targetEventKey);
				$scope.refresh();
			}).
			error(function(data){
				alert("error");
			});
	};

	$scope.update = function(){
		alert("update");
		$http.get('/event?action=query').success(function(data){
			$scope.events = data;
		});
	}
}

function EventCtrl($scope, $http,$timeout){
	$scope.EventDetail = function(){
		alert("event name:" + $scope.event.name);
	}

	$scope.EventDelete = function(){
		if (confirm('イベントを削除してよろしいですか？')) {
			var refreshFlg = false;
			$http.get('/event?action=delete&eventKey='+$scope.event.keyStr).
				success(function(data){
					targetEventKey = $scope.event.keyStr;
					rootScope.refresh();
				}).
				error(function(data){
					alert("プレイヤーの削除に失敗しました。");
				});
		}
	}
}