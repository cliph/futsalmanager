var oldChange = 0;
var eventKey = "";
var rootScope = null;
function EventDetailCtrl($scope, $http,$timeout,$routeParams){
	rootScope = $scope;
	$scope.event = null;
	console.log("EventDetailCtrl eventKey:"+$routeParams.eventKey);
	eventKey = $routeParams.eventKey;
	$http.get('/event?action=query&eventKey='+eventKey).success(function(data){
		console.log("http success");
		$scope.event = data[0];
		oldChange = data[0].change;
	});

	$http.get('/eventPlayer?action=list').success(function(data){
		console.log("http success");
		$scope.players = data;
	});

	$scope.getMemberCnt = function(){
		var count = 0;
	    angular.forEach($scope.players, function(player) {
	      count += player.member ? 1 : 0;
	    });
	    return count;
	}

	var cnt = 0;
	$scope.message = "";
	$scope.refresh = function(){
		$scope.message = "waiting for update:"+cnt+"sec";
		cnt = cnt + 1;
		$timeout(function(){
			$http.get('/event?action=query&eventKey='+eventKey).success(function(data){
				$scope.event = data[0];
				console.log("data.length:"+data.length);
			});
			var finish = false;
			console.log("event.change:"+$scope.event.change+" oldChange:"+oldChange);
			if($scope.event.change>oldChange){
				console.log("changed.");
				finish = true;
			}
			if(!finish&&cnt<10){
				console.log("continue:"+cnt);
				$scope.refresh();
				oldChange = $scope.event.change;
			}else{
				console.log("finish:"+cnt);
				$scope.reset();
			}
		},3000);
	}

	$scope.reset = function(){
		$scope.message="";
		cnt = 0;
	}
	$scope.updateEvent = function(){
		$http.get('/event?action=update&eventKey='+$scope.event.keyStr+'&name='+$scope.event.name+
				'&group='+$scope.event.group+'&sex='+$scope.event.sex+'&age='+$scope.event.age+
				'&level='+$scope.event.level+'&position='+$scope.event.position+'&introduce='+$scope.event.introduce).
			success(function(data){
				switch(data.resultCode){
				case 0:
					console.log("dataSize:"+data.length);
					$scope.refresh();
					break;
				default:
					console.log(data.message);
					break;
				}
			}).
			error(function(data){
				alert("error");
			});
	};
}



function EventPlayerCtrl($scope,$http){
	$scope.teams = ['Red','Blue','Grean','Orange','White'];
	$scope.memberChecked = function() {
		  if($scope.player.member){
				$http.get('/eventPlayer?action=create&playerKeyStr='+$scope.player.playerKeyStr+'&eventKeyStr='+$scope.event.keyStr+'&name='+$scope.player.name+
						'&group='+$scope.player.group+'&sex='+$scope.player.sex+'&age='+$scope.player.age+
						'&level='+$scope.player.level+'&position='+$scope.player.position).
					success(function(data){
						switch(data.resultCode){
						case 0:
							console.log("dataSize:"+data.length);
							break;
						default:
							alert("error resultCode:"+data.resultCode+" message:"+data.message);
							console.log(data.message);
							break;
						}
					}).
					error(function(data){
						alert("error");
					});
		  }else{
			  	$scope.player.teamName="";
				$http.get('/eventPlayer?action=delete&eventPlayerKeyStr='+$scope.player.eventPlayerKeyStr).
				success(function(data){
					console.log("dataSize:"+data.length);
				}).
				error(function(data){
					alert("イベントプレイヤーの削除に失敗しました。");
				});
		  }
	};
	$scope.teamChange = function(){
		if($scope.player.teamName){
			$http.get('/eventPlayer?action=update&eventPlayerKey='+$scope.player.eventPlayerKeyStr+'&teamName='+$scope.player.teamName).
			success(function(data){
				console.log("update success");
			}).
			error(function(data){
				alert("チーム変更に失敗しました。");
			});
		}
	}
}