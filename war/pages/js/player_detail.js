var oldChange = 0;
var playerKey = "";
function PlayerDetailCtrl($scope, $http,$timeout,$routeParams){
	$scope.player = null;
	console.log("PlayerDetailCtrl playerKey:"+$routeParams.playerKey);
	playerKey = $routeParams.playerKey;
	$http.get('/player?action=query&playerKey='+playerKey).success(function(data){
		console.log("http success");
		$scope.player = data[0];
		oldChange = data[0].change;
	});

	var cnt = 0;
	$scope.message = "";
	$scope.refresh = function(){
		$scope.message = "waiting for update:"+cnt+"sec";
		cnt = cnt + 1;
		$timeout(function(){
			$http.get('/player?action=query&playerKey='+playerKey).success(function(data){
				$scope.player = data[0];
				console.log("data.length:"+data.length);
			});
			var finish = false;
			console.log("player.change:"+$scope.player.change+" oldChange:"+oldChange);
			if($scope.player.change>oldChange){
				console.log("changed.");
				finish = true;
			}
			if(!finish&&cnt<10){
				console.log("continue:"+cnt);
				$scope.refresh();
				oldChange = $scope.player.change;
			}else{
				console.log("finish:"+cnt);
				$scope.reset();
			}
		},3000);
	}

	$scope.reset = function(){
		$scope.message="";
		cnt = 0;
	}
	$scope.updatePlayer = function(){
		$http.get('/player?action=update&playerKey='+$scope.player.keyStr+'&name='+$scope.player.name+
				'&group='+$scope.player.group+'&sex='+$scope.player.sex+'&age='+$scope.player.age+
				'&level='+$scope.player.level+'&position='+$scope.player.position+'&introduce='+$scope.player.introduce).
			success(function(data){
				switch(data.resultCode){
				case 0:
					console.log("dataSize:"+data.length);
					$scope.refresh();
					break;
				default:
					console.log(data.message);
					break;
				}
			}).
			error(function(data){
				alert("error");
			});
	};
}