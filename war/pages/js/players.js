var rootScope = null;
var targetPlayerKey = "";
function PlayerListCtrl($scope, $http,$timeout){
	rootScope = $scope;
	$http.get('/player?action=query').success(function(data){
		$scope.players = data;
	});

	var cnt = 0;
	$scope.message = "";
	$scope.refresh = function(){
		$scope.message = "waiting for update:"+cnt;
		cnt = cnt + 1;
		$timeout(function(){
			$http.get('/player?action=query').success(function(data){
				$scope.players = data;
				console.log("data.length:"+data.length);
			});
			var finish = true;
			for(var player in $scope.players){
				console.log("player.keyStr:"+player.keyStr+" targetPlayerKey:"+targetPlayerKey);
				if(player.keyStr==targetPlayerKey){
					console.log("targetExist");
					finish = false;
				}
			}
			if(!finish&&cnt<10){
				console.log("continue:"+cnt);
				$scope.refresh();
			}else{
				console.log("finish:"+cnt);
				$scope.reset();
			}
		},3000);
	}

	$scope.reset = function(){
		$scope.message="";
		targetPlayerKey="";
		cnt = 0;
	}
	$scope.newPlayer = function(){
		var refreshFlg = false;
		$http.get('/player?action=create&name='+$scope.newPlayerName+'&group='+$scope.newPlayerGroup+'&level='+$scope.newPlayerLevel).
			success(function(data){
				console.log("dataSize:"+data.length);
				targetPlayerKey = data.keyStr;
				console.log("targetPlayerKeyNew:"+targetPlayerKey);
				$scope.refresh();
			}).
			error(function(data){
				alert("error");
			});
	};

	$scope.update = function(){
		alert("update");
		$http.get('/player?action=query').success(function(data){
			$scope.players = data;
		});
	}
}

function PlayerCtrl($scope, $http,$timeout){
	$scope.PlayerDetail = function(){
		alert("player name:" + $scope.player.name);
	}

	$scope.PlayerDelete = function(){
		if (confirm('プレイヤーを削除してよろしいですか？')) {
			var refreshFlg = false;
			$http.get('/player?action=delete&playerKey='+$scope.player.keyStr).
				success(function(data){
					targetPlayerKey = $scope.player.keyStr;
					rootScope.refresh();
				}).
				error(function(data){
					alert("プレイヤーの削除に失敗しました。");
				});
		}
	}
}