var rootScope = null;
var eventKeyStr = null;
function GameListCtrl($scope, $http,$timeout,$routeParams){
	eventKeyStr = $routeParams.eventKey;
	rootScope = $scope;
	$scope.eventDetail = null;
	$http.get('/event?action=query&eventKey='+eventKeyStr).success(function(data){
		$scope.eventDetail = data[0];
	});

	$http.get('/eventPlayer?action=teams&eventKeyStr='+eventKeyStr).success(function(data){
		$scope.teams = data;
	});

	$http.get('/game?action=query&eventKeyStr='+eventKeyStr).success(function(data){
		$scope.games = data;
	});
	
}

function GameCtrl($scope,$http,$timeout){

}