package net.cliph.gae.futsalmanager.controller;

import org.slim3.tester.ControllerTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class TeamControllerTest extends ControllerTestCase {

    @Test
    public void run() throws Exception {
        tester.start("/Team");
        TeamController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));
    }
}
