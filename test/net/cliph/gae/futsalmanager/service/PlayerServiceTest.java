package net.cliph.gae.futsalmanager.service;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PlayerServiceTest extends AppEngineTestCase {

    private PlayerService service = new PlayerService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }
}
