package net.cliph.gae.futsalmanager.service;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class EventPlayerServiceTest extends AppEngineTestCase {

    private EventPlayerService service = new EventPlayerService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }
}
