package net.cliph.gae.futsalmanager.service;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class TeamServiceTest extends AppEngineTestCase {

    private TeamService service = new TeamService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }
}
