package net.cliph.gae.futsalmanager.service;

import java.util.List;
import java.util.logging.Logger;

import net.cliph.gae.futsalmanager.beans.ResultBean;
import net.cliph.gae.futsalmanager.controller.PlayerController;
import net.cliph.gae.futsalmanager.meta.EventMeta;
import net.cliph.gae.futsalmanager.meta.PlayerMeta;
import net.cliph.gae.futsalmanager.model.Event;
import net.cliph.gae.futsalmanager.model.Player;

import org.slim3.datastore.Datastore;
import org.slim3.util.BeanUtil;
import org.slim3.util.RequestMap;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;


public class EventService {

    static final Logger logger = Logger.getLogger(EventService.class.getName());
    public ResultBean create(RequestMap rMap) {
        ResultBean r = new ResultBean();
        Event e = new Event();
        BeanUtil.copy(rMap, e);

        try{
            Transaction tx = Datastore.beginTransaction();
            Datastore.put(tx,e);
            tx.commit();
            r.setKeyStr(e.getKeyStr());
            logger.info(r.getKeyStr());
        }catch(Exception ex){
            r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
            r.setMessage(ex.getMessage());
        }
        
        return r;
    }

    EventMeta meta = EventMeta.get();
    public ResultBean update(RequestMap rMap) {
        ResultBean r = new ResultBean();
        if(rMap.get("eventKeyStr")!=null){
                String keyStr = rMap.get("eventKeyStr").toString();
                Event e = Datastore.query(meta).filter(meta.keyStr.equal(keyStr)).asSingle();
            if(e!=null){
                BeanUtil.copy(rMap, e);
                e.setChange(e.getChange()+1);
                try{
                    Transaction tx = Datastore.beginTransaction();
                    Datastore.put(tx,e);
                    tx.commit();
                    r.setKeyStr(e.getKeyStr());
                }catch(Exception ex){
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    r.setMessage(ex.getMessage());
                }
            }else{
                r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                r.setMessage("Event is null");
            }
        }else{
            r.setResultCode(ResultBean.RESULT_INVALID_PRAM);
            r.setMessage("PlayerKey is null");
        }
        return r;
    }
    public ResultBean delete(RequestMap rMap) {
        ResultBean r = new ResultBean();
        if(rMap.get("eventKeyStr")!=null){
            Key key = null;
            try{
                key = KeyFactory.stringToKey(rMap.get("eventKeyStr").toString());
            }catch(IllegalArgumentException e){
                r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                r.setMessage(e.getMessage());
            }
            if(key!=null){
                try{
                    Transaction tx = Datastore.beginTransaction();
                    Datastore.delete(tx,key);
                    tx.commit();
                }catch(Exception e){
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    r.setMessage(e.getMessage());
                }
            }else{
                r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                r.setMessage("key is null");
            }
        }else{
            r.setResultCode(ResultBean.RESULT_INVALID_PRAM);
            r.setMessage("EventKey is null");
        }
        return r;
    }
    public List<Event> query(RequestMap rMap) {
        if(rMap.get("eventKeyStr")!=null){
            String eventKey = rMap.get("eventKeyStr").toString();
            return Datastore.query(meta).filter(meta.keyStr.equal(eventKey)).asList();
        }else if(rMap.get("eventName")!=null){
            String eventName = rMap.get("eventName").toString();
            return Datastore.query(meta).filter(meta.eventName.startsWith(eventName)).asList();
        }else{
            return Datastore.query(meta).limit(50).asList();
        }

    }

}
