package net.cliph.gae.futsalmanager.service;

import java.util.List;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import net.cliph.gae.futsalmanager.beans.ResultBean;
import net.cliph.gae.futsalmanager.controller.PlayerController;
import net.cliph.gae.futsalmanager.meta.PlayerMeta;
import net.cliph.gae.futsalmanager.model.Player;

import org.slim3.datastore.Datastore;
import org.slim3.util.BeanUtil;
import org.slim3.util.RequestMap;

public class PlayerService {
    static final Logger logger = Logger.getLogger(PlayerController.class.getName());
    public ResultBean create(RequestMap rMap) {
        ResultBean r = new ResultBean();
        Player p = new Player();
        BeanUtil.copy(rMap, p);

        try{
            Transaction tx = Datastore.beginTransaction();
            Datastore.put(tx,p);
            tx.commit();
            r.setKeyStr(p.getKeyStr());
            logger.info(r.getKeyStr());
        }catch(Exception e){
            r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
            r.setMessage(e.getMessage());
        }

        return r;
    }

    PlayerMeta meta = PlayerMeta.get();
    public ResultBean update(RequestMap rMap) {
        ResultBean r = new ResultBean();
        if(rMap.get("playerKey")!=null){
                String keyStr = rMap.get("playerKey").toString();
                Player p = Datastore.query(meta).filter(meta.keyStr.equal(keyStr)).asSingle();
            if(p!=null){
                BeanUtil.copy(rMap, p);
                p.setChange(p.getChange()+1);
                try{
                    Transaction tx = Datastore.beginTransaction();
                    Datastore.put(tx,p);
                    tx.commit();
                    r.setKeyStr(p.getKeyStr());
                }catch(Exception e){
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    r.setMessage(e.getMessage());
                }
            }else{
                r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                r.setMessage("Player is null");
            }
        }else{
            r.setResultCode(ResultBean.RESULT_INVALID_PRAM);
            r.setMessage("PlayerKey is null");
        }
        return r;
    }
    public ResultBean delete(RequestMap rMap) {
        ResultBean r = new ResultBean();
        if(rMap.get("playerKey")!=null){
            Key key = null;
            try{
                key = KeyFactory.stringToKey(rMap.get("playerKey").toString());
            }catch(IllegalArgumentException e){
                r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                r.setMessage(e.getMessage());
            }
            if(key!=null){
                try{
                    Transaction tx = Datastore.beginTransaction();
                    Datastore.delete(tx,key);
                    tx.commit();
                }catch(Exception e){
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    r.setMessage(e.getMessage());
                }
            }else{
                r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                r.setMessage("key is null");
            }
        }else{
            r.setResultCode(ResultBean.RESULT_INVALID_PRAM);
            r.setMessage("PlayerKey is null");
        }
        return r;
    }
    public List<Player> query(RequestMap rMap) {
        if(rMap.get("playerKey")!=null){
            String playerKey = rMap.get("playerKey").toString();
            return Datastore.query(meta).filter(meta.keyStr.equal(playerKey)).asList();
        }else if(rMap.get("name")!=null){
            String name = rMap.get("name").toString();
            return Datastore.query(meta).filter(meta.name.startsWith(name)).asList();
        }else if(rMap.get("group")!=null){
            String group = rMap.get("group").toString();
            return Datastore.query(meta).filter(meta.group.startsWith(group)).asList();
        }else{
            return Datastore.query(meta).limit(50).asList();
        }

    }

}
