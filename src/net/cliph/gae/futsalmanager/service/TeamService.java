package net.cliph.gae.futsalmanager.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import net.cliph.gae.futsalmanager.beans.ResultBean;
import net.cliph.gae.futsalmanager.controller.PlayerController;
import net.cliph.gae.futsalmanager.meta.EventPlayerMeta;
import net.cliph.gae.futsalmanager.meta.PlayerMeta;
import net.cliph.gae.futsalmanager.meta.TeamMeta;
import net.cliph.gae.futsalmanager.model.EventPlayer;
import net.cliph.gae.futsalmanager.model.Player;
import net.cliph.gae.futsalmanager.model.Team;

import org.slim3.datastore.Datastore;
import org.slim3.util.BeanUtil;
import org.slim3.util.RequestMap;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;


public class TeamService {
    static final Logger logger = Logger.getLogger(TeamService.class.getName());
    public ResultBean create(RequestMap rMap) {

        Team t = new Team();
        BeanUtil.copy(rMap, t);

        Transaction tx = Datastore.beginTransaction();
        Datastore.put(tx,t);
        tx.commit();

        ResultBean r = new ResultBean();
        r.setKeyStr(t.getTeamKeyStr());
        return r;
    }

    TeamMeta meta = TeamMeta.get();
    public ResultBean update(RequestMap rMap) {
        ResultBean result = new ResultBean();
        if(rMap.get("teamKeyStr")!=null){
                String teamKeyStr = rMap.get("teamKeyStr").toString();
                Team t = Datastore.query(meta).filter(meta.teamKeyStr.equal(teamKeyStr)).asSingle();
            if(t!=null){
                BeanUtil.copy(rMap, t);
                Transaction tx = Datastore.beginTransaction();
                Datastore.put(tx,t);
                tx.commit();
                result.setKeyStr(t.getTeamKeyStr());
            }else{
                result.setMessage("team update failed");
                result.setResultCode(ResultBean.RESULT_ACTION_FAILED);

            }
        }else{
            result.setMessage("team update failed");
            result.setResultCode(ResultBean.RESULT_INVALID_ACTION);
        }
        return result;
    }
    public ResultBean delete(RequestMap rMap) {
        ResultBean result = new ResultBean();
        if(rMap.get("teamKeyStr")!=null){
            Key key = null;
            try{
                key = KeyFactory.stringToKey(rMap.get("teamKeyStr").toString());
            }catch(IllegalArgumentException e){
                result.setResultCode(ResultBean.RESULT_INVALID_KEY);
                result.setMessage(e.getMessage());
                return result;
            }

            if(key!=null){
                try{
                Transaction tx = Datastore.beginTransaction();
                Datastore.delete(tx,key);
                tx.commit();
                }catch(Exception e){
                    result.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    result.setMessage(e.getMessage());
                    return result;
                }
            }else{
                result.setMessage("failed to parse key");
                result.setResultCode(ResultBean.RESULT_ACTION_FAILED);
            }
        }else{
            result.setMessage("'playerKey'param missing");
            result.setResultCode(ResultBean.RESULT_INVALID_PRAM);
        }
        return result;
    }
    public List<Team> query(RequestMap rMap) {
        if(rMap.get("teamKeyStr")!=null){
            String teamKeyStr = rMap.get("teamKeyStr").toString();
            return Datastore.query(meta).filter(meta.teamKeyStr.equal(teamKeyStr)).asList();
        }else if(rMap.get("teamName")!=null){
            String teamName = rMap.get("teamName").toString();
            return Datastore.query(meta).filter(meta.teamName.equal(teamName)).asList();
        }else if(rMap.get("eventKeyStr")!=null){
            logger.info("eventKeyStr!=null");
            String eventKeyStr = rMap.get("eventKeyStr").toString();
            return Datastore.query(meta).filter(meta.eventKeyStr.equal(eventKeyStr)).asList();
        }else{
            return Datastore.query(meta).limit(50).asList();
        }
    }

    EventPlayerMeta epMeta = EventPlayerMeta.get();
    PlayerMeta pMeta = PlayerMeta.get();
    public List<Player> getTeamMember(RequestMap rMap){
        String teamKeyStr = rMap.get("teamKeyStr").toString();
        List<Player> pList = new ArrayList<Player>();
        List<EventPlayer> epList = Datastore.query(epMeta).filter(epMeta.teamKeyStr.equal(teamKeyStr)).asList();
        for(EventPlayer ep : epList){
            Player p = Datastore.query(pMeta).filter(pMeta.keyStr.equal(ep.getPlayerKeyStr())).asSingle();
            if(p!=null){
                pList.add(p);
            }else{
                logger.info("Player is null");
            }
        }
        logger.info("pListSize :" + pList.size());
        return pList;
    }

}
