package net.cliph.gae.futsalmanager.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import net.cliph.gae.futsalmanager.beans.EventPlayerBean;
import net.cliph.gae.futsalmanager.beans.ResultBean;
import net.cliph.gae.futsalmanager.beans.TeamBean;
import net.cliph.gae.futsalmanager.controller.EventPlayerController;
import net.cliph.gae.futsalmanager.meta.EventPlayerMeta;
import net.cliph.gae.futsalmanager.meta.PlayerMeta;
import net.cliph.gae.futsalmanager.model.EventPlayer;
import net.cliph.gae.futsalmanager.model.Player;

import org.slim3.datastore.Datastore;
import org.slim3.util.BeanUtil;
import org.slim3.util.RequestMap;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;


public class EventPlayerService {
    static final Logger logger = Logger.getLogger(EventPlayerService.class.getName());

    public ResultBean create(RequestMap rMap) {
        ResultBean r = new ResultBean();
        EventPlayer ep = new EventPlayer();
        BeanUtil.copy(rMap, ep);

        try{
            Transaction tx = Datastore.beginTransaction();
            Datastore.put(tx,ep);
            tx.commit();
            r.setKeyStr(ep.getKeyStr());
            logger.info(r.getKeyStr());
        }catch(Exception e){
            r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
            r.setMessage(e.getMessage());
        }

        return r;
    }

    EventPlayerMeta meta = EventPlayerMeta.get();
    public ResultBean update(RequestMap rMap) {
        ResultBean r = new ResultBean();
        if(rMap.get("eventPlayerKey")!=null){
                String keyStr = rMap.get("eventPlayerKey").toString();
                EventPlayer ep = Datastore.query(meta).filter(meta.keyStr.equal(keyStr)).asSingle();
                logger.info("rMap.toString():"+rMap.toString());
            if(ep!=null){

                BeanUtil.copy(rMap, ep);
                ep.setChange(ep.getChange()+1);
                try{
                    Transaction tx = Datastore.beginTransaction();
                    Datastore.put(tx,ep);
                    tx.commit();
                    r.setKeyStr(ep.getKeyStr());
                }catch(Exception e){
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    r.setMessage(e.getMessage());
                }
            }else{
                r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                r.setMessage("EventPlayer is null");
            }
        }else{
            r.setResultCode(ResultBean.RESULT_INVALID_PRAM);
            r.setMessage("eventPlayerKey is null");
        }
        return r;
    }

    public ResultBean delete(RequestMap rMap) {
        ResultBean r = new ResultBean();
        if(rMap.get("eventPlayerKeyStr")!=null){
            Key key = null;
            try{
                key = KeyFactory.stringToKey(rMap.get("eventPlayerKeyStr").toString());
            }catch(IllegalArgumentException e){
                r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                r.setMessage(e.getMessage());
            }
            if(key!=null){
                try{
                    Transaction tx = Datastore.beginTransaction();
                    Datastore.delete(tx,key);
                    tx.commit();
                }catch(Exception e){
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    r.setMessage(e.getMessage());
                }
            }else{
                r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                r.setMessage("key is null");
            }
        }else{
            r.setResultCode(ResultBean.RESULT_INVALID_PRAM);
            r.setMessage("EventPlayerKey is null");
        }
        return r;
    }

    public List<EventPlayer> query(RequestMap rMap) {

        if(rMap.get("eventPlayerKey")!=null){
            String eventPlayerKey = rMap.get("eventPlayerKey").toString();
            return Datastore.query(meta).filter(meta.keyStr.equal(eventPlayerKey)).asList();
        }else if(rMap.get("eventKeyStr")!=null){
            String eventKeyStr = rMap.get("eventKeyStr").toString();
            logger.info("eventKeyStr:"+eventKeyStr);
            if(rMap.get("team")!=null){
                String team = rMap.get("team").toString();
                logger.info("team:"+team);
                return Datastore.query(meta).filter(meta.eventKeyStr.equal(eventKeyStr)).filter(meta.teamName.equal(team)).asList();
            }else{
                return Datastore.query(meta).filter(meta.eventKeyStr.equal(eventKeyStr)).asList();
            }
        }else{
            return Datastore.query(meta).asList();
        }
    }

    public HashMap<String,TeamBean> getTeamList(RequestMap rMap){
        if(rMap==null||rMap.get("eventKeyStr")==null) return null;
        List<EventPlayer> epList = Datastore.query(meta).filter(meta.eventKeyStr.equal(rMap.get("eventKeyStr").toString())).asList();
        logger.info("eventKeyStr:"+rMap.get("eventKeyStr").toString() + " epList:"+epList.toString());
        HashMap<String,TeamBean> teamMap = new HashMap<String,TeamBean>();

        for(EventPlayer ep : epList){
            TeamBean teamBeanTemp = teamMap.get(ep.getTeamName());
            if(teamBeanTemp==null){
                teamMap.put(ep.getTeamName(), new TeamBean(ep.getTeamName()));
            }else{
                teamMap.put(ep.getTeamName(), teamBeanTemp.addMemberCount());
            }
        }
        logger.info("teamMap:"+teamMap.toString());

        return teamMap;
    }

    PlayerMeta pMeta = PlayerMeta.get();
    public List<EventPlayerBean> listForChoice(){
        List<EventPlayerBean> epBeanList = new ArrayList<EventPlayerBean>();
        List<Player> playerList = Datastore.query(pMeta).asList();
        for(Player p : playerList){
            List<EventPlayer> epList = Datastore.query(meta).filter(meta.playerKeyStr.equal(p.getKeyStr())).asList();
            if(epList==null||epList.size()==0){
                EventPlayerBean epBean = new EventPlayerBean(p,false,null);
                epBeanList.add(epBean);
            }else{
                for(EventPlayer ep : epList){
                    EventPlayerBean epBean = new EventPlayerBean(p,true,ep);
                    epBeanList.add(epBean);
                }
            }
        }
        return epBeanList;
    }

}
