package net.cliph.gae.futsalmanager.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import net.cliph.gae.futsalmanager.beans.GameFactory;
import net.cliph.gae.futsalmanager.beans.ResultBean;
import net.cliph.gae.futsalmanager.beans.TeamBean;
import net.cliph.gae.futsalmanager.controller.EventPlayerController;
import net.cliph.gae.futsalmanager.meta.EventPlayerMeta;
import net.cliph.gae.futsalmanager.meta.GameMeta;
import net.cliph.gae.futsalmanager.model.EventPlayer;
import net.cliph.gae.futsalmanager.model.Game;

import org.slim3.datastore.Datastore;
import org.slim3.util.BeanUtil;
import org.slim3.util.RequestMap;

import com.google.appengine.api.datastore.Transaction;


public class GameService {
    static final Logger logger = Logger.getLogger(GameService.class.getName());

    EventPlayerMeta epMeta = EventPlayerMeta.get();
    public ResultBean matchMake(RequestMap rMap){
        ResultBean r = new ResultBean();
        String eventKeyStr = rMap.get("eventKeyStr").toString();
        
        List<EventPlayer> epList = Datastore.query(epMeta).filter(epMeta.eventKeyStr.equal(eventKeyStr)).asList();
        
        List<String> teamList = new ArrayList<String>();
        
        for(EventPlayer ep : epList){
            if(!teamList.contains(ep.getTeamName())){
                teamList.add(ep.getTeamName());
            }
        }
        logger.info("teamList:"+teamList.toString());
        
        List<String> gameIdList = new ArrayList<String>();
        
        for(String home : teamList){
            for(String away : teamList){
                if(!home.equalsIgnoreCase(away)&&!gameIdList.contains(home+":"+away)&&!gameIdList.contains(away+":"+home)){
                    gameIdList.add(home+":"+away);
                }
            }
        }
        
        List<Game> gList = GameFactory.getGameList(gameIdList, eventKeyStr);
        
        for(Game g : gList){
            try{
                Transaction tx = Datastore.beginTransaction();
                Datastore.put(tx,g);
                tx.commit();
                r.setKeyStr(g.getKeyStr());
                logger.info(r.getKeyStr());
            }catch(Exception e){
                r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                r.setMessage(e.getMessage());
            }
        }
        
        return r;
    }
    
    GameMeta gMeta = GameMeta.get();
    public List<Game> query(RequestMap rMap) {

        if(rMap.get("eventKeyStr")!=null){
            String eventKeyStr = rMap.get("eventKeyStr").toString();
            return Datastore.query(gMeta).filter(gMeta.eventKeyStr.equal(eventKeyStr)).asList();
        }else if(rMap.get("teamName")!=null){
            String teamName = rMap.get("teamName").toString();
            logger.info("teamName:"+teamName);
            List<Game> gameListHome = Datastore.query(gMeta).filter(gMeta.homeTeam.equal(teamName)).asList();
            List<Game> gameListAway = Datastore.query(gMeta).filter(gMeta.awayTeam.equal(teamName)).asList();
            List<Game> gameList = new ArrayList<Game>();
            if(gameListHome!=null){
                gameList.addAll(gameListHome);
            }
            if(gameListHome!=null){
                gameList.addAll(gameListAway);
            }
            return gameList;
        }else{
            return null;
        }
    }
    
}
