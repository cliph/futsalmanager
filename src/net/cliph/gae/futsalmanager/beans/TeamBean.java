package net.cliph.gae.futsalmanager.beans;

public class TeamBean {
    private static final String TAG = TeamBean.class.getSimpleName();

    public TeamBean(String teamName){
        setTeamName(teamName);
    }

    private String teamName;
    private int memberCount;

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    public TeamBean addMemberCount(){
        this.memberCount++;
        return this;
    }
}
