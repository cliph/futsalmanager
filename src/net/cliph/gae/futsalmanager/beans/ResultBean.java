package net.cliph.gae.futsalmanager.beans;

import java.util.List;
import net.cliph.gae.futsalmanager.model.*;
public class ResultBean {
    public static final int RESULT_SUCCESS = 0;
    public static final int RESULT_INVALID_ACTION = 1;
    public static final int RESULT_ACTION_FAILED = 2;
    public static final int RESULT_INVALID_KEY = 3;
    public static final int RESULT_INVALID_PRAM = 4;

    private int resultCode;
    private String message;
    private String keyStr;
    private List<Player> pList;

    public ResultBean(){
        resultCode = 0;
        message = "OK";
        keyStr = null;
        pList =null;
    }

    public int getResultCode() {
        return resultCode;
    }
    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    public List<Player> getpList() {
        return pList;
    }

    public void setpList(List<Player> pList) {
        this.pList = pList;
    }

}
