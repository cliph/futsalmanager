package net.cliph.gae.futsalmanager.beans;

import net.cliph.gae.futsalmanager.model.EventPlayer;
import net.cliph.gae.futsalmanager.model.Player;

public class EventPlayerBean {
    private String playerKeyStr;
    private String eventPlayerKeyStr;
    private String name;
    private String group;
    private int sex;
    private int age;
    private int level;
    private boolean isMember;
    private String teamName;

    public EventPlayerBean(Player player,boolean isMember,EventPlayer ep){
        setPlayerKeyStr(player.getKeyStr());
        setEventPlayerKeyStr(ep==null?null:ep.getKeyStr());
        setName(player.getName());
        setGroup(player.getGroup());
        setSex(player.getSex());
        setAge(player.getAge());
        setLevel(player.getLevel());
        setMember(isMember);
        setTeamName(ep==null?null:ep.getTeamName());
    }

    public String getPlayerKeyStr() {
        return playerKeyStr;
    }
    public void setPlayerKeyStr(String playerKeyStr) {
        this.playerKeyStr = playerKeyStr;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getGroup() {
        return group;
    }
    public void setGroup(String group) {
        this.group = group;
    }
    public int getSex() {
        return sex;
    }
    public void setSex(int sex) {
        this.sex = sex;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public int getLevel() {
        return level;
    }
    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean isMember) {
        this.isMember = isMember;
    }

    public String getEventPlayerKeyStr() {
        return eventPlayerKeyStr;
    }

    public void setEventPlayerKeyStr(String eventPlayerKeyStr) {
        this.eventPlayerKeyStr = eventPlayerKeyStr;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

}
