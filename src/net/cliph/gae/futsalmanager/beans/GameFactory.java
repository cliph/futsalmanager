package net.cliph.gae.futsalmanager.beans;

import java.util.ArrayList;
import java.util.List;

import net.cliph.gae.futsalmanager.model.Game;

public class GameFactory {
    private static final String TAG = GameFactory.class.getSimpleName();
    
    public static List<Game> getGameList(List<String> gameIds,String eventKeyStr){
        List<Game> gameList = new ArrayList<Game>();
        
        int counter = 0;
        for(String gameId :gameIds){
            String[] teamNames = gameId.split(":");
            counter++;
            gameList.add(new Game(eventKeyStr,counter,teamNames[0],teamNames[1]));
        }
        return gameList;
    }
}
