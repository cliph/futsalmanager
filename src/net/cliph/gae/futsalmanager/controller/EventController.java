package net.cliph.gae.futsalmanager.controller;

import java.util.List;
import java.util.logging.Logger;

import net.arnx.jsonic.JSON;
import net.cliph.gae.futsalmanager.beans.ResultBean;
import net.cliph.gae.futsalmanager.model.Event;
import net.cliph.gae.futsalmanager.model.Player;
import net.cliph.gae.futsalmanager.service.EventService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.util.RequestMap;

public class EventController extends Controller {
    static final Logger logger = Logger.getLogger(EventController.class.getName());
    @Override
    public Navigation run() throws Exception {
        EventService eService = new EventService();
        RequestMap rMap = new RequestMap(request);
        JSON jsonObj = new JSON();
        jsonObj.setDateFormat("yyyy-MM-dd");
        String json = null;
        if(rMap.get("action")!=null){
            if(rMap.get("action").toString().equalsIgnoreCase("create")){
                ResultBean r = eService.create(rMap);
                if(r==null){
                    r.setMessage("event create failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                }
                json = jsonObj.format(r);
            }else if(rMap.get("action").toString().equalsIgnoreCase("update")){
                ResultBean r = eService.update(rMap);
                if(r==null){
                    r.setMessage("event update failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                }
                json = jsonObj.format(r);
            }else if(rMap.get("action").toString().equalsIgnoreCase("delete")){
                ResultBean r = eService.delete(rMap);
                if(r==null){
                    r.setMessage("event delete failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                }
                json = jsonObj.format(r);
            }else if(rMap.get("action").toString().equalsIgnoreCase("query")){
                List<Event> eList = eService.query(rMap);
                if(eList==null){
                    ResultBean r = new ResultBean();
                    r.setMessage("event query failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    json = jsonObj.format(r);
                }else{
                    json = jsonObj.format(eList);
                }
            }else{
                ResultBean result = new ResultBean();
                result.setMessage("invalid action");
                result.setResultCode(ResultBean.RESULT_INVALID_ACTION);
                json = jsonObj.format(result);
            }
        }else{
            ResultBean result = new ResultBean();
            result.setMessage("action pram missing");
            result.setResultCode(ResultBean.RESULT_INVALID_PRAM);
            json = jsonObj.format(result);
        }

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().println(json);
        logger.info(json);
        return null;
    }
}
