package net.cliph.gae.futsalmanager.controller;

import java.util.List;

import net.arnx.jsonic.JSON;
import net.cliph.gae.futsalmanager.beans.ResultBean;
import net.cliph.gae.futsalmanager.model.Match;
import net.cliph.gae.futsalmanager.service.MatchService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.util.RequestMap;

public class MatchController extends Controller {

    @Override
    public Navigation run() throws Exception {
        MatchService mService = new MatchService();
        List<Match> mList = mService.doGet(new RequestMap(request));

        String json;
        if(mList!=null){
            json = JSON.encode(mList);
        }else{
            ResultBean result = new ResultBean();
            result.setMessage("no character");
            result.setResultCode(1);
            json = JSON.encode(result);
        }

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().println(json);

        return null;
    }
}
