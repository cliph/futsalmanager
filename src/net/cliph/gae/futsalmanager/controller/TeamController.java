package net.cliph.gae.futsalmanager.controller;

import java.util.List;
import java.util.logging.Logger;

import net.arnx.jsonic.JSON;
import net.cliph.gae.futsalmanager.beans.ResultBean;
import net.cliph.gae.futsalmanager.model.Player;
import net.cliph.gae.futsalmanager.model.Team;
import net.cliph.gae.futsalmanager.service.TeamService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.util.RequestMap;

public class TeamController extends Controller {
    static final Logger logger = Logger.getLogger(TeamService.class.getName());
    @Override
    public Navigation run() throws Exception {
        TeamService tService = new TeamService();
        RequestMap rMap = new RequestMap(request);

        String json = null;
        if(rMap.get("action")!=null){
            if(rMap.get("action").toString().equalsIgnoreCase("create")){
                ResultBean r = tService.create(rMap);
                if(r==null){
                    r = new ResultBean();
                    r.setMessage("team create failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                }
                json = JSON.encode(r);
            }else if(rMap.get("action").toString().equalsIgnoreCase("update")){
                ResultBean r = tService.update(rMap);
                if(r==null){
                    r = new ResultBean();
                    r.setMessage("team update failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                }
                json = JSON.encode(r);
            }else if(rMap.get("action").toString().equalsIgnoreCase("delete")){
                ResultBean r = tService.delete(rMap);
                if(r==null){
                    r = new ResultBean();
                    r.setMessage("team delete failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                }
                json = JSON.encode(r);
            }else if(rMap.get("action").toString().equalsIgnoreCase("query")){
                List<Team> tList = tService.query(rMap);
                if(tList==null){
                    logger.info("tList==null");
                    ResultBean r = new ResultBean();
                    r.setMessage("team query failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    json = JSON.encode(r);
                }else{
                    logger.info("tList!=null");
                    json = JSON.encode(tList);
                }
            }else if(rMap.get("action").toString().equalsIgnoreCase("member")){
                List<Player> pList = tService.getTeamMember(rMap);
                if(pList==null){
                    ResultBean r = new ResultBean();
                    r.setMessage("team member failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    json = JSON.encode(r);
                }else{
                    json = JSON.encode(pList);
                }
            }else{
                ResultBean result = new ResultBean();
                result.setMessage("invalid action");
                result.setResultCode(ResultBean.RESULT_INVALID_ACTION);
                json = JSON.encode(result);
            }
        }else{
            ResultBean result = new ResultBean();
            result.setMessage("action pram missing");
            result.setResultCode(ResultBean.RESULT_INVALID_PRAM);
            json = JSON.encode(result);
        }

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().println(json);
        logger.info(json);
        return null;
    }
}
