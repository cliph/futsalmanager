package net.cliph.gae.futsalmanager.controller;

import java.util.List;
import java.util.logging.Logger;

import net.arnx.jsonic.JSON;
import net.cliph.gae.futsalmanager.beans.ResultBean;
import net.cliph.gae.futsalmanager.model.Game;
import net.cliph.gae.futsalmanager.model.Player;
import net.cliph.gae.futsalmanager.service.GameService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.util.RequestMap;

public class GameController extends Controller {
    static final Logger logger = Logger.getLogger(GameController.class.getName());
    @Override
    public Navigation run() throws Exception {
        
        GameService gService = new GameService();
        RequestMap rMap = new RequestMap(request);
        String json = null;
        if(rMap.get("action")!=null){
            if(rMap.get("action").toString().equalsIgnoreCase("matchmake")){
                ResultBean r = gService.matchMake(rMap);
                if(r==null){
                    r.setMessage("match Mailed failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                }
                json = JSON.encode(r);
            }else if(rMap.get("action").toString().equalsIgnoreCase("query")){
                List<Game> gList = gService.query(rMap);
                if(gList==null){
                    ResultBean r = new ResultBean();
                    r.setMessage("Game query failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    json = JSON.encode(r);
                }else{
                    json = JSON.encode(gList);
                }
            }else{
                ResultBean result = new ResultBean();
                result.setMessage("invalid action");
                result.setResultCode(ResultBean.RESULT_INVALID_ACTION);
                json = JSON.encode(result);
            }
        }else{
            ResultBean result = new ResultBean();
            result.setMessage("action pram missing");
            result.setResultCode(ResultBean.RESULT_INVALID_PRAM);
            json = JSON.encode(result);
        }

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().println(json);
        logger.info(json);
        return null;
    }
}
