package net.cliph.gae.futsalmanager.controller;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import net.arnx.jsonic.JSON;
import net.cliph.gae.futsalmanager.beans.EventPlayerBean;
import net.cliph.gae.futsalmanager.beans.ResultBean;
import net.cliph.gae.futsalmanager.beans.TeamBean;
import net.cliph.gae.futsalmanager.model.EventPlayer;
import net.cliph.gae.futsalmanager.model.Player;
import net.cliph.gae.futsalmanager.service.EventPlayerService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.util.RequestMap;

public class EventPlayerController extends Controller {
    static final Logger logger = Logger.getLogger(EventPlayerController.class.getName());
    @Override
    public Navigation run() throws Exception {
        EventPlayerService epService = new EventPlayerService();
        RequestMap rMap = new RequestMap(request);
        String json = null;
        if(rMap.get("action")!=null){
            if(rMap.get("action").toString().equalsIgnoreCase("create")){
                ResultBean r = epService.create(rMap);
                if(r==null){
                    r.setMessage("eventPlayer create failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                }
                json = JSON.encode(r);
            }else if(rMap.get("action").toString().equalsIgnoreCase("update")){
                ResultBean r = epService.update(rMap);
                if(r==null){
                    r.setMessage("eventPlayer update failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                }
                json = JSON.encode(r);
            }else if(rMap.get("action").toString().equalsIgnoreCase("delete")){
                ResultBean r =epService.delete(rMap);
                if(r==null){
                    r.setMessage("eventPlayer delete failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                }
                json = JSON.encode(r);
            }else if(rMap.get("action").toString().equalsIgnoreCase("query")){
                List<EventPlayer> epList = epService.query(rMap);
                if(epList==null){
                    ResultBean r = new ResultBean();
                    r.setMessage("player query failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    json = JSON.encode(r);
                }else{
                    json = JSON.encode(epList);
                }
            }else if(rMap.get("action").toString().equalsIgnoreCase("list")){
                List<EventPlayerBean> epBeanList = epService.listForChoice();
                if(epBeanList==null){
                    ResultBean r = new ResultBean();
                    r.setMessage("player list failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    json = JSON.encode(r);
                }else{
                    json = JSON.encode(epBeanList);
                }

            }else if(rMap.get("action").toString().equalsIgnoreCase("teams")){
                HashMap<String,TeamBean> teamMap = epService.getTeamList(rMap);
                if(teamMap==null){
                    ResultBean r = new ResultBean();
                    r.setMessage("team list failed");
                    r.setResultCode(ResultBean.RESULT_ACTION_FAILED);
                    json = JSON.encode(r);
                }else{
                    json = JSON.encode(teamMap);
                }
            }else{
                ResultBean result = new ResultBean();
                result.setMessage("invalid action");
                result.setResultCode(ResultBean.RESULT_INVALID_ACTION);
                json = JSON.encode(result);
            }
        }else{
            ResultBean result = new ResultBean();
            result.setMessage("action pram missing");
            result.setResultCode(ResultBean.RESULT_INVALID_PRAM);
            json = JSON.encode(result);
        }

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().println(json);
        logger.info(json);
        return null;
    }
}
