package net.cliph.gae.futsalmanager.controller;

import java.util.List;

import net.arnx.jsonic.JSON;
import net.cliph.gae.futsalmanager.beans.ResultBean;
import net.cliph.gae.futsalmanager.model.Goal;
import net.cliph.gae.futsalmanager.service.GoalService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.util.RequestMap;

public class GoalController extends Controller {

    @Override
    public Navigation run() throws Exception {
        GoalService gService = new GoalService();
        List<Goal> gList = gService.doGet(new RequestMap(request));

        String json;
        if(gList!=null){
            json = JSON.encode(gList);
        }else{
            ResultBean result = new ResultBean();
            result.setMessage("no character");
            result.setResultCode(1);
            json = JSON.encode(result);
        }

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().println(json);

        return null;
    }
}
