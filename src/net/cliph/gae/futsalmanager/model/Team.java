package net.cliph.gae.futsalmanager.model;

import java.io.Serializable;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import net.cliph.gae.futsalmanager.meta.TeamMeta;
import org.slim3.datastore.Attribute;
import org.slim3.datastore.Datastore;
import org.slim3.datastore.Model;

@Model(schemaVersion = 1)
public class Team implements Serializable {

    private static final long serialVersionUID = 1L;

    @Attribute(primaryKey = true)
    private Key key;

    @Attribute(version = true)
    private Long version;

    public Team(){
        this.key = Datastore.allocateId(TeamMeta.get());
        this.teamKeyStr =KeyFactory.keyToString(this.key);
        this.eventKeyStr = null;
        this.teamName = null;
    }

    String teamKeyStr;
    String eventKeyStr;
    String teamName;

    /**
     * Returns the key.
     *
     * @return the key
     */
    public Key getKey() {
        return key;
    }

    /**
     * Sets the key.
     *
     * @param key
     *            the key
     */
    public void setKey(Key key) {
        this.key = key;
    }

    /**
     * Returns the version.
     *
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Team other = (Team) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        return true;
    }

    public String getTeamKeyStr() {
        return teamKeyStr;
    }

    public void setTeamKeyStr(String teamKeyStr) {
        this.teamKeyStr = teamKeyStr;
    }

    public String getEventKeyStr() {
        return eventKeyStr;
    }

    public void setEventKeyStr(String eventKeyStr) {
        this.eventKeyStr = eventKeyStr;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
