package net.cliph.gae.futsalmanager.model;

import java.io.Serializable;

import com.google.appengine.api.datastore.Key;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.Model;

@Model(schemaVersion = 1)
public class Match implements Serializable {

    private static final long serialVersionUID = 1L;

    @Attribute(primaryKey = true)
    private Key key;

    @Attribute(version = true)
    private Long version;

    String keyStr;
    int matchNo;

    String teamKeyA;
    int teamGoalsA;
    Boolean teamWinA;
    int teamPointA;

    String teamKeyB;
    int teamGoalsB;
    Boolean teamWinB;
    int teamPointB;

    /**
     * Returns the key.
     *
     * @return the key
     */
    public Key getKey() {
        return key;
    }

    /**
     * Sets the key.
     *
     * @param key
     *            the key
     */
    public void setKey(Key key) {
        this.key = key;
    }

    /**
     * Returns the version.
     *
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Match other = (Match) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        return true;
    }

    public String getKeyStr() {
        return keyStr;
    }

    public void setKeyStr(String keyStr) {
        this.keyStr = keyStr;
    }

    public int getMatchNo() {
        return matchNo;
    }

    public void setMatchNo(int matchNo) {
        this.matchNo = matchNo;
    }

    public String getTeamKeyA() {
        return teamKeyA;
    }

    public void setTeamKeyA(String teamKeyA) {
        this.teamKeyA = teamKeyA;
    }

    public int getTeamGoalsA() {
        return teamGoalsA;
    }

    public void setTeamGoalsA(int teamGoalsA) {
        this.teamGoalsA = teamGoalsA;
    }

    public Boolean getTeamWinA() {
        return teamWinA;
    }

    public void setTeamWinA(Boolean teamWinA) {
        this.teamWinA = teamWinA;
    }

    public String getTeamKeyB() {
        return teamKeyB;
    }

    public void setTeamKeyB(String teamKeyB) {
        this.teamKeyB = teamKeyB;
    }

    public int getTeamGoalsB() {
        return teamGoalsB;
    }

    public void setTeamGoalsB(int teamGoalsB) {
        this.teamGoalsB = teamGoalsB;
    }

    public Boolean getTeamWinB() {
        return teamWinB;
    }

    public void setTeamWinB(Boolean teamWinB) {
        this.teamWinB = teamWinB;
    }

    public int getTeamPointA() {
        return teamPointA;
    }

    public void setTeamPointA(int teamPointA) {
        this.teamPointA = teamPointA;
    }

    public int getTeamPointB() {
        return teamPointB;
    }

    public void setTeamPointB(int teamPointB) {
        this.teamPointB = teamPointB;
    }
}
