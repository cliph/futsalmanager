package net.cliph.gae.futsalmanager.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-05-01 22:51:05")
/** */
public final class GameMeta extends org.slim3.datastore.ModelMeta<net.cliph.gae.futsalmanager.model.Game> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Game, java.lang.Integer> awayGoals = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Game, java.lang.Integer>(this, "awayGoals", "awayGoals", int.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Game> awayTeam = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Game>(this, "awayTeam", "awayTeam");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Game> eventKeyStr = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Game>(this, "eventKeyStr", "eventKeyStr");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Game, java.lang.Integer> gameNumber = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Game, java.lang.Integer>(this, "gameNumber", "gameNumber", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Game, java.lang.Integer> homeGoals = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Game, java.lang.Integer>(this, "homeGoals", "homeGoals", int.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Game> homeTeam = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Game>(this, "homeTeam", "homeTeam");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Game, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Game, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Game> keyStr = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Game>(this, "keyStr", "keyStr");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Game, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Game, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final GameMeta slim3_singleton = new GameMeta();

    /**
     * @return the singleton
     */
    public static GameMeta get() {
       return slim3_singleton;
    }

    /** */
    public GameMeta() {
        super("Game", net.cliph.gae.futsalmanager.model.Game.class);
    }

    @Override
    public net.cliph.gae.futsalmanager.model.Game entityToModel(com.google.appengine.api.datastore.Entity entity) {
        net.cliph.gae.futsalmanager.model.Game model = new net.cliph.gae.futsalmanager.model.Game();
        model.setAwayGoals(longToPrimitiveInt((java.lang.Long) entity.getProperty("awayGoals")));
        model.setAwayTeam((java.lang.String) entity.getProperty("awayTeam"));
        model.setEventKeyStr((java.lang.String) entity.getProperty("eventKeyStr"));
        model.setGameNumber(longToPrimitiveInt((java.lang.Long) entity.getProperty("gameNumber")));
        model.setHomeGoals(longToPrimitiveInt((java.lang.Long) entity.getProperty("homeGoals")));
        model.setHomeTeam((java.lang.String) entity.getProperty("homeTeam"));
        model.setKey(entity.getKey());
        model.setKeyStr((java.lang.String) entity.getProperty("keyStr"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        net.cliph.gae.futsalmanager.model.Game m = (net.cliph.gae.futsalmanager.model.Game) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("awayGoals", m.getAwayGoals());
        entity.setProperty("awayTeam", m.getAwayTeam());
        entity.setProperty("eventKeyStr", m.getEventKeyStr());
        entity.setProperty("gameNumber", m.getGameNumber());
        entity.setProperty("homeGoals", m.getHomeGoals());
        entity.setProperty("homeTeam", m.getHomeTeam());
        entity.setProperty("keyStr", m.getKeyStr());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        net.cliph.gae.futsalmanager.model.Game m = (net.cliph.gae.futsalmanager.model.Game) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        net.cliph.gae.futsalmanager.model.Game m = (net.cliph.gae.futsalmanager.model.Game) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        net.cliph.gae.futsalmanager.model.Game m = (net.cliph.gae.futsalmanager.model.Game) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        net.cliph.gae.futsalmanager.model.Game m = (net.cliph.gae.futsalmanager.model.Game) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.Game m = (net.cliph.gae.futsalmanager.model.Game) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        writer.setNextPropertyName("awayGoals");
        encoder0.encode(writer, m.getAwayGoals());
        if(m.getAwayTeam() != null){
            writer.setNextPropertyName("awayTeam");
            encoder0.encode(writer, m.getAwayTeam());
        }
        if(m.getEventKeyStr() != null){
            writer.setNextPropertyName("eventKeyStr");
            encoder0.encode(writer, m.getEventKeyStr());
        }
        writer.setNextPropertyName("gameNumber");
        encoder0.encode(writer, m.getGameNumber());
        writer.setNextPropertyName("homeGoals");
        encoder0.encode(writer, m.getHomeGoals());
        if(m.getHomeTeam() != null){
            writer.setNextPropertyName("homeTeam");
            encoder0.encode(writer, m.getHomeTeam());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getKeyStr() != null){
            writer.setNextPropertyName("keyStr");
            encoder0.encode(writer, m.getKeyStr());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected net.cliph.gae.futsalmanager.model.Game jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.Game m = new net.cliph.gae.futsalmanager.model.Game();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("awayGoals");
        m.setAwayGoals(decoder0.decode(reader, m.getAwayGoals()));
        reader = rootReader.newObjectReader("awayTeam");
        m.setAwayTeam(decoder0.decode(reader, m.getAwayTeam()));
        reader = rootReader.newObjectReader("eventKeyStr");
        m.setEventKeyStr(decoder0.decode(reader, m.getEventKeyStr()));
        reader = rootReader.newObjectReader("gameNumber");
        m.setGameNumber(decoder0.decode(reader, m.getGameNumber()));
        reader = rootReader.newObjectReader("homeGoals");
        m.setHomeGoals(decoder0.decode(reader, m.getHomeGoals()));
        reader = rootReader.newObjectReader("homeTeam");
        m.setHomeTeam(decoder0.decode(reader, m.getHomeTeam()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("keyStr");
        m.setKeyStr(decoder0.decode(reader, m.getKeyStr()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}