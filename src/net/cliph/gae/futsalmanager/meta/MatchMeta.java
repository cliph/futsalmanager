package net.cliph.gae.futsalmanager.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-05-01 22:50:21")
/** */
public final class MatchMeta extends org.slim3.datastore.ModelMeta<net.cliph.gae.futsalmanager.model.Match> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Match> keyStr = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Match>(this, "keyStr", "keyStr");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Integer> matchNo = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Integer>(this, "matchNo", "matchNo", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Integer> teamGoalsA = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Integer>(this, "teamGoalsA", "teamGoalsA", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Integer> teamGoalsB = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Integer>(this, "teamGoalsB", "teamGoalsB", int.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Match> teamKeyA = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Match>(this, "teamKeyA", "teamKeyA");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Match> teamKeyB = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Match>(this, "teamKeyB", "teamKeyB");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Integer> teamPointA = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Integer>(this, "teamPointA", "teamPointA", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Integer> teamPointB = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Integer>(this, "teamPointB", "teamPointB", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Boolean> teamWinA = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Boolean>(this, "teamWinA", "teamWinA", java.lang.Boolean.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Boolean> teamWinB = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Boolean>(this, "teamWinB", "teamWinB", java.lang.Boolean.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Match, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final MatchMeta slim3_singleton = new MatchMeta();

    /**
     * @return the singleton
     */
    public static MatchMeta get() {
       return slim3_singleton;
    }

    /** */
    public MatchMeta() {
        super("Match", net.cliph.gae.futsalmanager.model.Match.class);
    }

    @Override
    public net.cliph.gae.futsalmanager.model.Match entityToModel(com.google.appengine.api.datastore.Entity entity) {
        net.cliph.gae.futsalmanager.model.Match model = new net.cliph.gae.futsalmanager.model.Match();
        model.setKey(entity.getKey());
        model.setKeyStr((java.lang.String) entity.getProperty("keyStr"));
        model.setMatchNo(longToPrimitiveInt((java.lang.Long) entity.getProperty("matchNo")));
        model.setTeamGoalsA(longToPrimitiveInt((java.lang.Long) entity.getProperty("teamGoalsA")));
        model.setTeamGoalsB(longToPrimitiveInt((java.lang.Long) entity.getProperty("teamGoalsB")));
        model.setTeamKeyA((java.lang.String) entity.getProperty("teamKeyA"));
        model.setTeamKeyB((java.lang.String) entity.getProperty("teamKeyB"));
        model.setTeamPointA(longToPrimitiveInt((java.lang.Long) entity.getProperty("teamPointA")));
        model.setTeamPointB(longToPrimitiveInt((java.lang.Long) entity.getProperty("teamPointB")));
        model.setTeamWinA((java.lang.Boolean) entity.getProperty("teamWinA"));
        model.setTeamWinB((java.lang.Boolean) entity.getProperty("teamWinB"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        net.cliph.gae.futsalmanager.model.Match m = (net.cliph.gae.futsalmanager.model.Match) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("keyStr", m.getKeyStr());
        entity.setProperty("matchNo", m.getMatchNo());
        entity.setProperty("teamGoalsA", m.getTeamGoalsA());
        entity.setProperty("teamGoalsB", m.getTeamGoalsB());
        entity.setProperty("teamKeyA", m.getTeamKeyA());
        entity.setProperty("teamKeyB", m.getTeamKeyB());
        entity.setProperty("teamPointA", m.getTeamPointA());
        entity.setProperty("teamPointB", m.getTeamPointB());
        entity.setProperty("teamWinA", m.getTeamWinA());
        entity.setProperty("teamWinB", m.getTeamWinB());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        net.cliph.gae.futsalmanager.model.Match m = (net.cliph.gae.futsalmanager.model.Match) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        net.cliph.gae.futsalmanager.model.Match m = (net.cliph.gae.futsalmanager.model.Match) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        net.cliph.gae.futsalmanager.model.Match m = (net.cliph.gae.futsalmanager.model.Match) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        net.cliph.gae.futsalmanager.model.Match m = (net.cliph.gae.futsalmanager.model.Match) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.Match m = (net.cliph.gae.futsalmanager.model.Match) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getKeyStr() != null){
            writer.setNextPropertyName("keyStr");
            encoder0.encode(writer, m.getKeyStr());
        }
        writer.setNextPropertyName("matchNo");
        encoder0.encode(writer, m.getMatchNo());
        writer.setNextPropertyName("teamGoalsA");
        encoder0.encode(writer, m.getTeamGoalsA());
        writer.setNextPropertyName("teamGoalsB");
        encoder0.encode(writer, m.getTeamGoalsB());
        if(m.getTeamKeyA() != null){
            writer.setNextPropertyName("teamKeyA");
            encoder0.encode(writer, m.getTeamKeyA());
        }
        if(m.getTeamKeyB() != null){
            writer.setNextPropertyName("teamKeyB");
            encoder0.encode(writer, m.getTeamKeyB());
        }
        writer.setNextPropertyName("teamPointA");
        encoder0.encode(writer, m.getTeamPointA());
        writer.setNextPropertyName("teamPointB");
        encoder0.encode(writer, m.getTeamPointB());
        if(m.getTeamWinA() != null){
            writer.setNextPropertyName("teamWinA");
            encoder0.encode(writer, m.getTeamWinA());
        }
        if(m.getTeamWinB() != null){
            writer.setNextPropertyName("teamWinB");
            encoder0.encode(writer, m.getTeamWinB());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected net.cliph.gae.futsalmanager.model.Match jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.Match m = new net.cliph.gae.futsalmanager.model.Match();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("keyStr");
        m.setKeyStr(decoder0.decode(reader, m.getKeyStr()));
        reader = rootReader.newObjectReader("matchNo");
        m.setMatchNo(decoder0.decode(reader, m.getMatchNo()));
        reader = rootReader.newObjectReader("teamGoalsA");
        m.setTeamGoalsA(decoder0.decode(reader, m.getTeamGoalsA()));
        reader = rootReader.newObjectReader("teamGoalsB");
        m.setTeamGoalsB(decoder0.decode(reader, m.getTeamGoalsB()));
        reader = rootReader.newObjectReader("teamKeyA");
        m.setTeamKeyA(decoder0.decode(reader, m.getTeamKeyA()));
        reader = rootReader.newObjectReader("teamKeyB");
        m.setTeamKeyB(decoder0.decode(reader, m.getTeamKeyB()));
        reader = rootReader.newObjectReader("teamPointA");
        m.setTeamPointA(decoder0.decode(reader, m.getTeamPointA()));
        reader = rootReader.newObjectReader("teamPointB");
        m.setTeamPointB(decoder0.decode(reader, m.getTeamPointB()));
        reader = rootReader.newObjectReader("teamWinA");
        m.setTeamWinA(decoder0.decode(reader, m.getTeamWinA()));
        reader = rootReader.newObjectReader("teamWinB");
        m.setTeamWinB(decoder0.decode(reader, m.getTeamWinB()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}