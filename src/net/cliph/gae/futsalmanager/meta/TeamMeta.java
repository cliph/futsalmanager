package net.cliph.gae.futsalmanager.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-05-01 22:50:20")
/** */
public final class TeamMeta extends org.slim3.datastore.ModelMeta<net.cliph.gae.futsalmanager.model.Team> {

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Team> eventKeyStr = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Team>(this, "eventKeyStr", "eventKeyStr");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Team, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Team, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Team> teamKeyStr = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Team>(this, "teamKeyStr", "teamKeyStr");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Team> teamName = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Team>(this, "teamName", "teamName");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Team, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Team, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final TeamMeta slim3_singleton = new TeamMeta();

    /**
     * @return the singleton
     */
    public static TeamMeta get() {
       return slim3_singleton;
    }

    /** */
    public TeamMeta() {
        super("Team", net.cliph.gae.futsalmanager.model.Team.class);
    }

    @Override
    public net.cliph.gae.futsalmanager.model.Team entityToModel(com.google.appengine.api.datastore.Entity entity) {
        net.cliph.gae.futsalmanager.model.Team model = new net.cliph.gae.futsalmanager.model.Team();
        model.setEventKeyStr((java.lang.String) entity.getProperty("eventKeyStr"));
        model.setKey(entity.getKey());
        model.setTeamKeyStr((java.lang.String) entity.getProperty("teamKeyStr"));
        model.setTeamName((java.lang.String) entity.getProperty("teamName"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        net.cliph.gae.futsalmanager.model.Team m = (net.cliph.gae.futsalmanager.model.Team) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("eventKeyStr", m.getEventKeyStr());
        entity.setProperty("teamKeyStr", m.getTeamKeyStr());
        entity.setProperty("teamName", m.getTeamName());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        net.cliph.gae.futsalmanager.model.Team m = (net.cliph.gae.futsalmanager.model.Team) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        net.cliph.gae.futsalmanager.model.Team m = (net.cliph.gae.futsalmanager.model.Team) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        net.cliph.gae.futsalmanager.model.Team m = (net.cliph.gae.futsalmanager.model.Team) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        net.cliph.gae.futsalmanager.model.Team m = (net.cliph.gae.futsalmanager.model.Team) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.Team m = (net.cliph.gae.futsalmanager.model.Team) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getEventKeyStr() != null){
            writer.setNextPropertyName("eventKeyStr");
            encoder0.encode(writer, m.getEventKeyStr());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getTeamKeyStr() != null){
            writer.setNextPropertyName("teamKeyStr");
            encoder0.encode(writer, m.getTeamKeyStr());
        }
        if(m.getTeamName() != null){
            writer.setNextPropertyName("teamName");
            encoder0.encode(writer, m.getTeamName());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected net.cliph.gae.futsalmanager.model.Team jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.Team m = new net.cliph.gae.futsalmanager.model.Team();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("eventKeyStr");
        m.setEventKeyStr(decoder0.decode(reader, m.getEventKeyStr()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("teamKeyStr");
        m.setTeamKeyStr(decoder0.decode(reader, m.getTeamKeyStr()));
        reader = rootReader.newObjectReader("teamName");
        m.setTeamName(decoder0.decode(reader, m.getTeamName()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}