package net.cliph.gae.futsalmanager.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-05-01 22:50:21")
/** */
public final class EventMeta extends org.slim3.datastore.ModelMeta<net.cliph.gae.futsalmanager.model.Event> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Event, java.lang.Integer> change = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Event, java.lang.Integer>(this, "change", "change", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Event, java.util.Date> eventDate = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Event, java.util.Date>(this, "eventDate", "eventDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Event> eventName = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Event>(this, "eventName", "eventName");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Event, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Event, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Event> keyStr = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Event>(this, "keyStr", "keyStr");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Event> place = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Event>(this, "place", "place");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Event, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Event, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final EventMeta slim3_singleton = new EventMeta();

    /**
     * @return the singleton
     */
    public static EventMeta get() {
       return slim3_singleton;
    }

    /** */
    public EventMeta() {
        super("Event", net.cliph.gae.futsalmanager.model.Event.class);
    }

    @Override
    public net.cliph.gae.futsalmanager.model.Event entityToModel(com.google.appengine.api.datastore.Entity entity) {
        net.cliph.gae.futsalmanager.model.Event model = new net.cliph.gae.futsalmanager.model.Event();
        model.setChange(longToPrimitiveInt((java.lang.Long) entity.getProperty("change")));
        model.setEventDate((java.util.Date) entity.getProperty("eventDate"));
        model.setEventName((java.lang.String) entity.getProperty("eventName"));
        model.setKey(entity.getKey());
        model.setKeyStr((java.lang.String) entity.getProperty("keyStr"));
        model.setPlace((java.lang.String) entity.getProperty("place"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        net.cliph.gae.futsalmanager.model.Event m = (net.cliph.gae.futsalmanager.model.Event) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("change", m.getChange());
        entity.setProperty("eventDate", m.getEventDate());
        entity.setProperty("eventName", m.getEventName());
        entity.setProperty("keyStr", m.getKeyStr());
        entity.setProperty("place", m.getPlace());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        net.cliph.gae.futsalmanager.model.Event m = (net.cliph.gae.futsalmanager.model.Event) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        net.cliph.gae.futsalmanager.model.Event m = (net.cliph.gae.futsalmanager.model.Event) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        net.cliph.gae.futsalmanager.model.Event m = (net.cliph.gae.futsalmanager.model.Event) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        net.cliph.gae.futsalmanager.model.Event m = (net.cliph.gae.futsalmanager.model.Event) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.Event m = (net.cliph.gae.futsalmanager.model.Event) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        writer.setNextPropertyName("change");
        encoder0.encode(writer, m.getChange());
        if(m.getEventDate() != null){
            writer.setNextPropertyName("eventDate");
            encoder0.encode(writer, m.getEventDate());
        }
        if(m.getEventName() != null){
            writer.setNextPropertyName("eventName");
            encoder0.encode(writer, m.getEventName());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getKeyStr() != null){
            writer.setNextPropertyName("keyStr");
            encoder0.encode(writer, m.getKeyStr());
        }
        if(m.getPlace() != null){
            writer.setNextPropertyName("place");
            encoder0.encode(writer, m.getPlace());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected net.cliph.gae.futsalmanager.model.Event jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.Event m = new net.cliph.gae.futsalmanager.model.Event();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("change");
        m.setChange(decoder0.decode(reader, m.getChange()));
        reader = rootReader.newObjectReader("eventDate");
        m.setEventDate(decoder0.decode(reader, m.getEventDate()));
        reader = rootReader.newObjectReader("eventName");
        m.setEventName(decoder0.decode(reader, m.getEventName()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("keyStr");
        m.setKeyStr(decoder0.decode(reader, m.getKeyStr()));
        reader = rootReader.newObjectReader("place");
        m.setPlace(decoder0.decode(reader, m.getPlace()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}