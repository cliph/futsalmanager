package net.cliph.gae.futsalmanager.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-05-01 22:50:21")
/** */
public final class EventPlayerMeta extends org.slim3.datastore.ModelMeta<net.cliph.gae.futsalmanager.model.EventPlayer> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, java.lang.Integer> age = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, java.lang.Integer>(this, "age", "age", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, java.lang.Integer> change = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, java.lang.Integer>(this, "change", "change", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, java.util.Date> entryDate = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, java.util.Date>(this, "entryDate", "entryDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer> eventKeyStr = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer>(this, "eventKeyStr", "eventKeyStr");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer> group = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer>(this, "group", "group");

    /** */
    public final org.slim3.datastore.UnindexedAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, com.google.appengine.api.datastore.Blob> icon = new org.slim3.datastore.UnindexedAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, com.google.appengine.api.datastore.Blob>(this, "icon", "icon", com.google.appengine.api.datastore.Blob.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer> keyStr = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer>(this, "keyStr", "keyStr");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, java.lang.Integer> level = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, java.lang.Integer>(this, "level", "level", int.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer> name = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer>(this, "name", "name");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer> playerKeyStr = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer>(this, "playerKeyStr", "playerKeyStr");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer> position = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer>(this, "position", "position");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, java.lang.Integer> sex = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, java.lang.Integer>(this, "sex", "sex", int.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer> teamKeyStr = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer>(this, "teamKeyStr", "teamKeyStr");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer> teamName = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer>(this, "teamName", "teamName");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.EventPlayer, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final EventPlayerMeta slim3_singleton = new EventPlayerMeta();

    /**
     * @return the singleton
     */
    public static EventPlayerMeta get() {
       return slim3_singleton;
    }

    /** */
    public EventPlayerMeta() {
        super("EventPlayer", net.cliph.gae.futsalmanager.model.EventPlayer.class);
    }

    @Override
    public net.cliph.gae.futsalmanager.model.EventPlayer entityToModel(com.google.appengine.api.datastore.Entity entity) {
        net.cliph.gae.futsalmanager.model.EventPlayer model = new net.cliph.gae.futsalmanager.model.EventPlayer();
        model.setAge(longToPrimitiveInt((java.lang.Long) entity.getProperty("age")));
        model.setChange(longToPrimitiveInt((java.lang.Long) entity.getProperty("change")));
        model.setEntryDate((java.util.Date) entity.getProperty("entryDate"));
        model.setEventKeyStr((java.lang.String) entity.getProperty("eventKeyStr"));
        model.setGroup((java.lang.String) entity.getProperty("group"));
        model.setIcon((com.google.appengine.api.datastore.Blob) entity.getProperty("icon"));
        model.setKey(entity.getKey());
        model.setKeyStr((java.lang.String) entity.getProperty("keyStr"));
        model.setLevel(longToPrimitiveInt((java.lang.Long) entity.getProperty("level")));
        model.setName((java.lang.String) entity.getProperty("name"));
        model.setPlayerKeyStr((java.lang.String) entity.getProperty("playerKeyStr"));
        model.setPosition((java.lang.String) entity.getProperty("position"));
        model.setSex(longToPrimitiveInt((java.lang.Long) entity.getProperty("sex")));
        model.setTeamKeyStr((java.lang.String) entity.getProperty("teamKeyStr"));
        model.setTeamName((java.lang.String) entity.getProperty("teamName"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        net.cliph.gae.futsalmanager.model.EventPlayer m = (net.cliph.gae.futsalmanager.model.EventPlayer) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("age", m.getAge());
        entity.setProperty("change", m.getChange());
        entity.setProperty("entryDate", m.getEntryDate());
        entity.setProperty("eventKeyStr", m.getEventKeyStr());
        entity.setProperty("group", m.getGroup());
        entity.setProperty("icon", m.getIcon());
        entity.setProperty("keyStr", m.getKeyStr());
        entity.setProperty("level", m.getLevel());
        entity.setProperty("name", m.getName());
        entity.setProperty("playerKeyStr", m.getPlayerKeyStr());
        entity.setProperty("position", m.getPosition());
        entity.setProperty("sex", m.getSex());
        entity.setProperty("teamKeyStr", m.getTeamKeyStr());
        entity.setProperty("teamName", m.getTeamName());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        net.cliph.gae.futsalmanager.model.EventPlayer m = (net.cliph.gae.futsalmanager.model.EventPlayer) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        net.cliph.gae.futsalmanager.model.EventPlayer m = (net.cliph.gae.futsalmanager.model.EventPlayer) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        net.cliph.gae.futsalmanager.model.EventPlayer m = (net.cliph.gae.futsalmanager.model.EventPlayer) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        net.cliph.gae.futsalmanager.model.EventPlayer m = (net.cliph.gae.futsalmanager.model.EventPlayer) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.EventPlayer m = (net.cliph.gae.futsalmanager.model.EventPlayer) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        writer.setNextPropertyName("age");
        encoder0.encode(writer, m.getAge());
        writer.setNextPropertyName("change");
        encoder0.encode(writer, m.getChange());
        if(m.getEntryDate() != null){
            writer.setNextPropertyName("entryDate");
            encoder0.encode(writer, m.getEntryDate());
        }
        if(m.getEventKeyStr() != null){
            writer.setNextPropertyName("eventKeyStr");
            encoder0.encode(writer, m.getEventKeyStr());
        }
        if(m.getGroup() != null){
            writer.setNextPropertyName("group");
            encoder0.encode(writer, m.getGroup());
        }
        if(m.getIcon() != null && m.getIcon().getBytes() != null){
            writer.setNextPropertyName("icon");
            encoder0.encode(writer, m.getIcon());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getKeyStr() != null){
            writer.setNextPropertyName("keyStr");
            encoder0.encode(writer, m.getKeyStr());
        }
        writer.setNextPropertyName("level");
        encoder0.encode(writer, m.getLevel());
        if(m.getName() != null){
            writer.setNextPropertyName("name");
            encoder0.encode(writer, m.getName());
        }
        if(m.getPlayerKeyStr() != null){
            writer.setNextPropertyName("playerKeyStr");
            encoder0.encode(writer, m.getPlayerKeyStr());
        }
        if(m.getPosition() != null){
            writer.setNextPropertyName("position");
            encoder0.encode(writer, m.getPosition());
        }
        writer.setNextPropertyName("sex");
        encoder0.encode(writer, m.getSex());
        if(m.getTeamKeyStr() != null){
            writer.setNextPropertyName("teamKeyStr");
            encoder0.encode(writer, m.getTeamKeyStr());
        }
        if(m.getTeamName() != null){
            writer.setNextPropertyName("teamName");
            encoder0.encode(writer, m.getTeamName());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected net.cliph.gae.futsalmanager.model.EventPlayer jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.EventPlayer m = new net.cliph.gae.futsalmanager.model.EventPlayer();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("age");
        m.setAge(decoder0.decode(reader, m.getAge()));
        reader = rootReader.newObjectReader("change");
        m.setChange(decoder0.decode(reader, m.getChange()));
        reader = rootReader.newObjectReader("entryDate");
        m.setEntryDate(decoder0.decode(reader, m.getEntryDate()));
        reader = rootReader.newObjectReader("eventKeyStr");
        m.setEventKeyStr(decoder0.decode(reader, m.getEventKeyStr()));
        reader = rootReader.newObjectReader("group");
        m.setGroup(decoder0.decode(reader, m.getGroup()));
        reader = rootReader.newObjectReader("icon");
        m.setIcon(decoder0.decode(reader, m.getIcon()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("keyStr");
        m.setKeyStr(decoder0.decode(reader, m.getKeyStr()));
        reader = rootReader.newObjectReader("level");
        m.setLevel(decoder0.decode(reader, m.getLevel()));
        reader = rootReader.newObjectReader("name");
        m.setName(decoder0.decode(reader, m.getName()));
        reader = rootReader.newObjectReader("playerKeyStr");
        m.setPlayerKeyStr(decoder0.decode(reader, m.getPlayerKeyStr()));
        reader = rootReader.newObjectReader("position");
        m.setPosition(decoder0.decode(reader, m.getPosition()));
        reader = rootReader.newObjectReader("sex");
        m.setSex(decoder0.decode(reader, m.getSex()));
        reader = rootReader.newObjectReader("teamKeyStr");
        m.setTeamKeyStr(decoder0.decode(reader, m.getTeamKeyStr()));
        reader = rootReader.newObjectReader("teamName");
        m.setTeamName(decoder0.decode(reader, m.getTeamName()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}