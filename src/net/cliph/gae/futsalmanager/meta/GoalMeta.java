package net.cliph.gae.futsalmanager.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-05-01 22:50:21")
/** */
public final class GoalMeta extends org.slim3.datastore.ModelMeta<net.cliph.gae.futsalmanager.model.Goal> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Goal, java.util.Date> goalDate = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Goal, java.util.Date>(this, "goalDate", "goalDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Goal, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Goal, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Goal> keyStr = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Goal>(this, "keyStr", "keyStr");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Goal> matchKey = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Goal>(this, "matchKey", "matchKey");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Goal> playerKey = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Goal>(this, "playerKey", "playerKey");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Goal> teamKey = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Goal>(this, "teamKey", "teamKey");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Goal, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Goal, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final GoalMeta slim3_singleton = new GoalMeta();

    /**
     * @return the singleton
     */
    public static GoalMeta get() {
       return slim3_singleton;
    }

    /** */
    public GoalMeta() {
        super("Goal", net.cliph.gae.futsalmanager.model.Goal.class);
    }

    @Override
    public net.cliph.gae.futsalmanager.model.Goal entityToModel(com.google.appengine.api.datastore.Entity entity) {
        net.cliph.gae.futsalmanager.model.Goal model = new net.cliph.gae.futsalmanager.model.Goal();
        model.setGoalDate((java.util.Date) entity.getProperty("goalDate"));
        model.setKey(entity.getKey());
        model.setKeyStr((java.lang.String) entity.getProperty("keyStr"));
        model.setMatchKey((java.lang.String) entity.getProperty("matchKey"));
        model.setPlayerKey((java.lang.String) entity.getProperty("playerKey"));
        model.setTeamKey((java.lang.String) entity.getProperty("teamKey"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        net.cliph.gae.futsalmanager.model.Goal m = (net.cliph.gae.futsalmanager.model.Goal) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("goalDate", m.getGoalDate());
        entity.setProperty("keyStr", m.getKeyStr());
        entity.setProperty("matchKey", m.getMatchKey());
        entity.setProperty("playerKey", m.getPlayerKey());
        entity.setProperty("teamKey", m.getTeamKey());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        net.cliph.gae.futsalmanager.model.Goal m = (net.cliph.gae.futsalmanager.model.Goal) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        net.cliph.gae.futsalmanager.model.Goal m = (net.cliph.gae.futsalmanager.model.Goal) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        net.cliph.gae.futsalmanager.model.Goal m = (net.cliph.gae.futsalmanager.model.Goal) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        net.cliph.gae.futsalmanager.model.Goal m = (net.cliph.gae.futsalmanager.model.Goal) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.Goal m = (net.cliph.gae.futsalmanager.model.Goal) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getGoalDate() != null){
            writer.setNextPropertyName("goalDate");
            encoder0.encode(writer, m.getGoalDate());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getKeyStr() != null){
            writer.setNextPropertyName("keyStr");
            encoder0.encode(writer, m.getKeyStr());
        }
        if(m.getMatchKey() != null){
            writer.setNextPropertyName("matchKey");
            encoder0.encode(writer, m.getMatchKey());
        }
        if(m.getPlayerKey() != null){
            writer.setNextPropertyName("playerKey");
            encoder0.encode(writer, m.getPlayerKey());
        }
        if(m.getTeamKey() != null){
            writer.setNextPropertyName("teamKey");
            encoder0.encode(writer, m.getTeamKey());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected net.cliph.gae.futsalmanager.model.Goal jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.Goal m = new net.cliph.gae.futsalmanager.model.Goal();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("goalDate");
        m.setGoalDate(decoder0.decode(reader, m.getGoalDate()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("keyStr");
        m.setKeyStr(decoder0.decode(reader, m.getKeyStr()));
        reader = rootReader.newObjectReader("matchKey");
        m.setMatchKey(decoder0.decode(reader, m.getMatchKey()));
        reader = rootReader.newObjectReader("playerKey");
        m.setPlayerKey(decoder0.decode(reader, m.getPlayerKey()));
        reader = rootReader.newObjectReader("teamKey");
        m.setTeamKey(decoder0.decode(reader, m.getTeamKey()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}