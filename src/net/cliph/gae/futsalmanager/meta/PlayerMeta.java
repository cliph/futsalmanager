package net.cliph.gae.futsalmanager.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-05-01 22:50:20")
/** */
public final class PlayerMeta extends org.slim3.datastore.ModelMeta<net.cliph.gae.futsalmanager.model.Player> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer> age = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer>(this, "age", "age", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer> change = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer>(this, "change", "change", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.util.Date> entryDate = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.util.Date>(this, "entryDate", "entryDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer> goals = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer>(this, "goals", "goals", int.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Player> group = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Player>(this, "group", "group");

    /** */
    public final org.slim3.datastore.UnindexedAttributeMeta<net.cliph.gae.futsalmanager.model.Player, com.google.appengine.api.datastore.Blob> icon = new org.slim3.datastore.UnindexedAttributeMeta<net.cliph.gae.futsalmanager.model.Player, com.google.appengine.api.datastore.Blob>(this, "icon", "icon", com.google.appengine.api.datastore.Blob.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Player> introduce = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Player>(this, "introduce", "introduce");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Player> keyStr = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Player>(this, "keyStr", "keyStr");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer> level = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer>(this, "level", "level", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer> matchs = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer>(this, "matchs", "matchs", int.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Player> name = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Player>(this, "name", "name");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Player> position = new org.slim3.datastore.StringAttributeMeta<net.cliph.gae.futsalmanager.model.Player>(this, "position", "position");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer> sex = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer>(this, "sex", "sex", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer> winEvents = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer>(this, "winEvents", "winEvents", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer> wins = new org.slim3.datastore.CoreAttributeMeta<net.cliph.gae.futsalmanager.model.Player, java.lang.Integer>(this, "wins", "wins", int.class);

    private static final PlayerMeta slim3_singleton = new PlayerMeta();

    /**
     * @return the singleton
     */
    public static PlayerMeta get() {
       return slim3_singleton;
    }

    /** */
    public PlayerMeta() {
        super("Player", net.cliph.gae.futsalmanager.model.Player.class);
    }

    @Override
    public net.cliph.gae.futsalmanager.model.Player entityToModel(com.google.appengine.api.datastore.Entity entity) {
        net.cliph.gae.futsalmanager.model.Player model = new net.cliph.gae.futsalmanager.model.Player();
        model.setAge(longToPrimitiveInt((java.lang.Long) entity.getProperty("age")));
        model.setChange(longToPrimitiveInt((java.lang.Long) entity.getProperty("change")));
        model.setEntryDate((java.util.Date) entity.getProperty("entryDate"));
        model.setGoals(longToPrimitiveInt((java.lang.Long) entity.getProperty("goals")));
        model.setGroup((java.lang.String) entity.getProperty("group"));
        model.setIcon((com.google.appengine.api.datastore.Blob) entity.getProperty("icon"));
        model.setIntroduce((java.lang.String) entity.getProperty("introduce"));
        model.setKey(entity.getKey());
        model.setKeyStr((java.lang.String) entity.getProperty("keyStr"));
        model.setLevel(longToPrimitiveInt((java.lang.Long) entity.getProperty("level")));
        model.setMatchs(longToPrimitiveInt((java.lang.Long) entity.getProperty("matchs")));
        model.setName((java.lang.String) entity.getProperty("name"));
        model.setPosition((java.lang.String) entity.getProperty("position"));
        model.setSex(longToPrimitiveInt((java.lang.Long) entity.getProperty("sex")));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        model.setWinEvents(longToPrimitiveInt((java.lang.Long) entity.getProperty("winEvents")));
        model.setWins(longToPrimitiveInt((java.lang.Long) entity.getProperty("wins")));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        net.cliph.gae.futsalmanager.model.Player m = (net.cliph.gae.futsalmanager.model.Player) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("age", m.getAge());
        entity.setProperty("change", m.getChange());
        entity.setProperty("entryDate", m.getEntryDate());
        entity.setProperty("goals", m.getGoals());
        entity.setProperty("group", m.getGroup());
        entity.setProperty("icon", m.getIcon());
        entity.setProperty("introduce", m.getIntroduce());
        entity.setProperty("keyStr", m.getKeyStr());
        entity.setProperty("level", m.getLevel());
        entity.setProperty("matchs", m.getMatchs());
        entity.setProperty("name", m.getName());
        entity.setProperty("position", m.getPosition());
        entity.setProperty("sex", m.getSex());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("winEvents", m.getWinEvents());
        entity.setProperty("wins", m.getWins());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        net.cliph.gae.futsalmanager.model.Player m = (net.cliph.gae.futsalmanager.model.Player) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        net.cliph.gae.futsalmanager.model.Player m = (net.cliph.gae.futsalmanager.model.Player) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        net.cliph.gae.futsalmanager.model.Player m = (net.cliph.gae.futsalmanager.model.Player) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        net.cliph.gae.futsalmanager.model.Player m = (net.cliph.gae.futsalmanager.model.Player) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.Player m = (net.cliph.gae.futsalmanager.model.Player) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        writer.setNextPropertyName("age");
        encoder0.encode(writer, m.getAge());
        writer.setNextPropertyName("change");
        encoder0.encode(writer, m.getChange());
        if(m.getEntryDate() != null){
            writer.setNextPropertyName("entryDate");
            encoder0.encode(writer, m.getEntryDate());
        }
        writer.setNextPropertyName("goals");
        encoder0.encode(writer, m.getGoals());
        if(m.getGroup() != null){
            writer.setNextPropertyName("group");
            encoder0.encode(writer, m.getGroup());
        }
        if(m.getIcon() != null && m.getIcon().getBytes() != null){
            writer.setNextPropertyName("icon");
            encoder0.encode(writer, m.getIcon());
        }
        if(m.getIntroduce() != null){
            writer.setNextPropertyName("introduce");
            encoder0.encode(writer, m.getIntroduce());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getKeyStr() != null){
            writer.setNextPropertyName("keyStr");
            encoder0.encode(writer, m.getKeyStr());
        }
        writer.setNextPropertyName("level");
        encoder0.encode(writer, m.getLevel());
        writer.setNextPropertyName("matchs");
        encoder0.encode(writer, m.getMatchs());
        if(m.getName() != null){
            writer.setNextPropertyName("name");
            encoder0.encode(writer, m.getName());
        }
        if(m.getPosition() != null){
            writer.setNextPropertyName("position");
            encoder0.encode(writer, m.getPosition());
        }
        writer.setNextPropertyName("sex");
        encoder0.encode(writer, m.getSex());
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.setNextPropertyName("winEvents");
        encoder0.encode(writer, m.getWinEvents());
        writer.setNextPropertyName("wins");
        encoder0.encode(writer, m.getWins());
        writer.endObject();
    }

    @Override
    protected net.cliph.gae.futsalmanager.model.Player jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        net.cliph.gae.futsalmanager.model.Player m = new net.cliph.gae.futsalmanager.model.Player();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("age");
        m.setAge(decoder0.decode(reader, m.getAge()));
        reader = rootReader.newObjectReader("change");
        m.setChange(decoder0.decode(reader, m.getChange()));
        reader = rootReader.newObjectReader("entryDate");
        m.setEntryDate(decoder0.decode(reader, m.getEntryDate()));
        reader = rootReader.newObjectReader("goals");
        m.setGoals(decoder0.decode(reader, m.getGoals()));
        reader = rootReader.newObjectReader("group");
        m.setGroup(decoder0.decode(reader, m.getGroup()));
        reader = rootReader.newObjectReader("icon");
        m.setIcon(decoder0.decode(reader, m.getIcon()));
        reader = rootReader.newObjectReader("introduce");
        m.setIntroduce(decoder0.decode(reader, m.getIntroduce()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("keyStr");
        m.setKeyStr(decoder0.decode(reader, m.getKeyStr()));
        reader = rootReader.newObjectReader("level");
        m.setLevel(decoder0.decode(reader, m.getLevel()));
        reader = rootReader.newObjectReader("matchs");
        m.setMatchs(decoder0.decode(reader, m.getMatchs()));
        reader = rootReader.newObjectReader("name");
        m.setName(decoder0.decode(reader, m.getName()));
        reader = rootReader.newObjectReader("position");
        m.setPosition(decoder0.decode(reader, m.getPosition()));
        reader = rootReader.newObjectReader("sex");
        m.setSex(decoder0.decode(reader, m.getSex()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        reader = rootReader.newObjectReader("winEvents");
        m.setWinEvents(decoder0.decode(reader, m.getWinEvents()));
        reader = rootReader.newObjectReader("wins");
        m.setWins(decoder0.decode(reader, m.getWins()));
        return m;
    }
}